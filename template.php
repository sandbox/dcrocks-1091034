<?php
/**
 * @file
 * Contains theme override functions and preprocess functions
 */
 
 // Helper function for theming main and secondary variables.
/**    The following (from Jeff Burnz) can be used to put a 'nav'
 *  around non-block' menus with theming classes
*/
function _theme_menu_variables($menu, $type) {
  $output = '<div id="' . $type . '-menu" class="nav"><nav>' . $menu . '</nav></div>';
  return $output;
} 
/**
 * Implements hook_html_head_alter().
/**
/** We are overwriting the default meta character type tag with HTML5 version.
*/  
function bakelite_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}
/**
 * Implements theme_menu_tree() to add clearfix to all menus.
 */
function bakelite_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}
/** Make some changes to breadcrumbs.
*/
function bakelite_breadcrumb($vars) {
	$breadcrumb = $vars['breadcrumb'];
     // Get rid of homepage entry
   	array_shift($breadcrumb); 
	// Bracket the list
	if (!empty($breadcrumb)) {      
	return  '<span class=brstart>&#9001 </span>' . implode(' &#171 ', $breadcrumb) . '<span class=brsshort> &#9002</span>';
	}
	return ''; 	      
}

/**
 * Changes the search form to use the "search" input element of HTML5.
 */
function bakelite_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

function bakelite_preprocess_html(&$variables, $hook) {
  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  if (!$variables['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        $section = 'node-add';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        $section = 'node-' . arg(2);
      }
    }
    $variables['classes_array'][] = drupal_html_class('section-' . $section);
  }
}   
/**
* Process page variables
*/

function bakelite_preprocess_page(&$vars) {
	
	// Get the 2 standard non-block menus.	
	if (isset($vars['main_menu'])) {
    $vars['primary_navigation'] = theme('links', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'class' => array('menu', 'primary-menu', 'clearfix'),
       ),
     'heading' => array(
     'text' => t('Primary navigation'),
     'level' => 'h2',
       'class' => array('element-invisible'),
     ) 
    ));
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_navigation'] = theme('links', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'class' => array('menu', 'secondary-menu', 'clearfix'),
     ),
     'heading' => array(
     'text' => t('Secondary navigation'),
     'level' => 'h2',
       'class' => array('element-invisible'),
     )
    ));
  }
    // Make them html5.  
  if (!empty($vars['primary_navigation'])) {
    $vars['primary_navigation'] = _theme_menu_variables($vars['primary_navigation'], 'primary');
  }
  if (!empty($vars['secondary_navigation'])) {
    $vars['secondary_navigation'] = _theme_menu_variables($vars['secondary_navigation'], 'secondary');
  }
     
  //  and make these vars.
  $vars['primary_local_tasks'] = menu_primary_local_tasks();
  $vars['secondary_local_tasks'] = menu_secondary_local_tasks();

 
	// get a menu to be added to Short Menu(command? menu) in a simple
	// sequence of anchors to allow a centered horizontal display without
	// using css that might break the flex-box model in webkit browsers.
	//
	//  Handling the special user menus is harder than it should be

	$vars['short_links'] = '';
	if ((theme_get_setting('short_menu')) == 'yes') {
	$menuname = theme_get_setting('short_menu_source');
	$shortlinks = '';
	$links = menu_load_links($menuname);
    foreach ($links as $key => $link) {
    if (!$link['hidden']) {   	// link enabled ?
    if (!((substr($link['link_path'],0,4)) == 'user')) {
    $shortlinks .= l($link['link_title'], $link['link_path'],$link['options']); }   //  process non 'user' menus
    else {     // user menu needs special handling
    if (($link['link_path'] == 'user/login') && (!$vars['logged_in']))
    {$shortlinks .= l($link['link_title'], $link['link_path'],$link['options']); }    
	if (($link['link_path'] == 'user/logout') && ($vars['logged_in']))
    {$shortlinks .= l($link['link_title'], $link['link_path'],$link['options']); }
    if (($link['link_path'] == 'user/register') || ($link['link_path'] == 'user/password'))
    {$shortlinks .= l($link['link_title'], $link['link_path'],$link['options']); }
    if (($link['link_path'] == 'user') && ($vars['logged_in']))
    {$shortlinks .= l($link['link_title'], $link['link_path'],$link['options']); }
    }   // end else 
    }   // end if 'hidden'
    }   // end foreach
    
	$vars['short_links'] = '<menu class="menubox">'.$shortlinks.'</menu>';
	}    //  end if short menu
}   // end preprocess page
/**
 * Override or insert variables into the block template.
 */
function bakelite_preprocess_block(&$vars) {
  // Only show block titles in sidebars or content area.
  $regr = substr($vars['block']->region,0,7);
    if ( !($regr == 'content' || $regr == 'sidebar')) {
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  }
      
  if (in_array($vars['block']->delta, array_keys(menu_get_menus()))) {
    $vars['theme_hook_suggestions'][] = 'block__menu';
  }
}    //   end preprocess block
function bakelite_preprocess_region(&$variables, $hook) {
  // Sidebar regions get some extra classes and a common template suggestion.
  if (strpos($variables['region'], 'sidebar_') === 0) {
    $variables['classes_array'][] = 'column';
    $variables['classes_array'][] = 'sidebar';
    $variables['theme_hook_suggestions'][] = 'region__sidebar';
    // Allow a region-specific template to override Zen's region--sidebar.
    $variables['theme_hook_suggestions'][] = 'region__' . $variables['region'];
  }
}     // end preprocess region
  
// Preprocess variables for node.tpl.php.
function bakelite_preprocess_node(&$vars) {
  $vars['datetime'] = format_date($vars['created'], 'custom', 'c');
  if (variable_get('node_submitted_' . $vars['node']->type, TRUE)) {
    $vars['submitted'] = t('Submitted by !username on !datetime',
      array(
        '!username' => $vars['name'],
        '!datetime' => '<time datetime="' . $vars['datetime'] . '" pubdate="pubdate">' . $vars['date'] . '</time>',
      )
    );
  }
  else {
    $vars['submitted'] = '';
  }
  $vars['unpublished'] = '';
  if (!$vars['status']) {
    $vars['unpublished'] = '<div class="unpublished">' . t('Unpublished') . '</div>';
  }
}     // end preprocess node

// Preprocess variables for comment.tpl.php.
function bakelite_preprocess_comment(&$vars) {
  $uri = entity_uri('comment', $vars['comment']);
  $uri['options'] += array('attributes' => array('rel' => 'bookmark'));
  $vars['title'] = l($vars['comment']->subject, $uri['path'], $uri['options']);
  $vars['permalink'] = l(t('Permalink'), $uri['path'], $uri['options']);
  $vars['created'] = '<span class="date-time permalink">' . l($vars['created'], $uri['path'], $uri['options']) . '</span>';
  $vars['datetime'] = format_date($vars['comment']->created, 'custom', 'c');
  $vars['unpublished'] = '';
  if ($vars['status'] == 'comment-unpublished') {
    $vars['unpublished'] = '<div class="unpublished">' . t('Unpublished') . '</div>';
  }
}   // end preprocess comment

// Preprocess variables for comment_wrapper.tpl.php
function bakelite_preprocess_comment_wrapper(&$vars) {
  if ($vars['node']->type == 'forum') {
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  }
}   // end preprocess comment_wrapper
