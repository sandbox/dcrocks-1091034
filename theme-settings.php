<?php
// $Id: theme-settings.php,v 1.2.4.18 2010/12/03 06:15:05 jmburnz Exp $

function bakelite_form_system_theme_settings_alter(&$form, $form_state) {
  // Site Menu
  $form['short_menu_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('short Menu'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	'#description'   => t('These settings allow you to add an additional menu to the default items in the short Menu.'),
  );
  $form['short_menu_settings']['short_menu'] = array(
    '#type' => 'select',
    '#title' => t('Add to short Menu'),
    '#default_value' => theme_get_setting('short_menu'),
    '#options' => array(
      'no' => t('No'),
      'yes' => t('Yes'),
    ),
  );
  $form['short_menu_settings']['short_menu_source'] = array(
    '#type' => 'select',
    '#title' => t('short Menu Source'),
    '#description' => t('Choose a menu to be added to the short Menu'),
    '#options' => menu_get_menus(),
    '#default_value' => theme_get_setting('short_menu_source'),
  );
}