<?php
// $Id: page.tpl.php,v 1.2 2010/07/28 01:40:39 dries Exp $

/**
 * @file
 *
 * The doctype, html, head and body tags are not in this template. Instead 
 * they can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['home_button']: User page switch control.
 * - $page['menu_bar']: Region allocated to menus.
 * - $page['directory']: Region for iconified site directory.
 * - $page['footer_top_left']: Region for footer items.
 * - $page['footer_top_right']: Region for footer items.
 * - $page['footer_bottom_left']: Region for footer items.
 * - $page['footer_bottom_right']: Region for footer items.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 */
 
/**    Bakelite theme variables
 * - $short_links (Array): A <menu> of <a> element commands *
 * @see bakelite_preprocess_page()
 */ 
?>

<!--                     Page Header                         -->

<header role="banner" classs="region region-header clearfix">

	<?php if ($logo): ?>
	<div id="logo">
	<a title="<?php print t('Home'); ?>" rel="home" href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"></a>
	</div>
	<?php endif; ?>

	<hgroup role="Page Title" class="clearfix">
	<?php if ($site_name): ?>
	<h1 class="site-name"><a title="<?php print t('Home'); ?>" rel="home" href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
	<?php endif; ?>
	<?php if ($site_slogan): ?>
	<h2 class="site-slogan"><?php print $site_slogan; ?></h2>
	<?php endif; ?>
	</hgroup>

	<?php print render($page['header']); ?>

<!--   Keep the Home button area at the bottom of the header    -->

	<section role="Navigation">  <!-- Hidden Home button region -->
	<h1 class="element-invisible">Call Home Page</h1>
	<div class="region region-home-button">
	<menu id="center"><a href="<?php print $front_page; ?>" title="Show Directory">push</a></menu>
	</div>
	</section>	<!-- /#home-button -->

</header>
<!--    If you needed another menu area you might put it here   -->

	<?php if ($menubar = render($page['menu_bar'])): ?>
	<h2 class="element-invisible">Menu Bar</h2>		
	<?php print $menubar; ?>
	<?php endif; ?>

<!--   The placement of sidebar_first, sidebar_second, featured and
       highlighted is rather arbitrary here as the base theme
       doesn't currently use them. Modify to meet your needs and
       don't forget to unhide in .info file.
-->
	<?php if ($featured = render($page['featured'])): ?>
		<div id="featured" class="featured">
		<h2 class="element-invisible">Featured</h2>		
		<?php print $featured; ?>
		</div>
	<?php endif; ?>

	<?php if ($highlighted = render($page['highlighted'])): ?>
		<div id="highlighted" class="highlighted">
		<h2 class="element-invisible">Highlighted</h2>		
		<?php print $highlighted; ?>
		</div>
	<?php endif; ?>

<!--                 Message area Popup                    
          It makes sense to always have these immediately above
          the content area.
-->

	<?php print $messages; ?>
	<?php print render($page['help']); ?>

<!--                 Content Area                          -->
<div id="column-container" class="clearfix">

	<?php if ($sidebar1 = render($page['sidebar_first'])): ?>
	    <div id="sidebar-first">
		<h2 class="element-invisible">First Sidebar</h2>		
		<?php print $sidebar1; ?>
		</div>
	<?php endif; ?>

	<div id="content-column" >
	<h1 class="element-invisible">Content Views</h1>
	

	<?php if ($breadcrumb): ?>
	<div id="breadcrumb" class="clearfix">
	<h2 class="element-invisible">You are Here</h2>	
	<?php print $breadcrumb; ?>
	</div>
    <?php endif; ?>

<!--                  View wrapper
     The design philosophy of this theme requires that the
     actual 'content region' should EITHER have site content OR
     the directory displayed to actually make sense
-->

	<?php if (!$is_front): ?>
	<div id="viewportA" class="clearfix">  

	<?php print render($title_prefix); ?>

	<?php if ($title): ?>
        <h2 class="title" id="page-title">
          <?php print $title; ?>
        </h2>
	<?php endif; ?>

	<?php print render($title_suffix); ?>

	<?php if ($tabs): ?>
          <?php print render($tabs); ?>
	<?php endif; ?>

	<?php if ($action_links = render($action_links)): ?>
        <ul class="action-links">
          <?php print $action_links; ?>
        </ul>
	<?php endif; ?>

	<?php print render($page['content']); ?>

	<?php print $feed_icons; ?>

	</div>  <!---   /#viewportA  -->
	<?php endif; ?>

	<?php if($is_front): ?>
	<div id="viewportB" class="clearfix">
	
	<?php if ($directory = render($page['directory'])): ?>
	<section role="navigation">
	<h1 class="element-invisible">Site Directory</h1>
	<?php print $directory; ?>
	</section> 
	<?php endif; ?>

<!--    This could either be on top of or the bottom of the
      content-column area, but the same width. You may have to make
      adjustments if you add sidebars.
-->

	<?php if ($short_links): ?>
	<section role="Navigation">
	<h1 class="element-invisible">Display Short Menu</h1>
	<div id="short-menu" class="clearfix">   <!-- Menu wrapper -->
	<div id="menu-box"> <!-- need extra div becaue of IE -->
	<?php print $short_links; ?>
	</div>
	</div>  <!-- /#short-menu -->
	</section> <!--  /#section-->
	<?php endif; ?>
	
	</div>     <!-- /#viewportB   -->
	<?php endif; ?>
	
</div> <!-- /#content-column -->

	<?php if ($sidebar2 = render($page['sidebar_second'])): ?>
		<div id="sidebar-second">
		<h2 class="element-invisible">Second Sidebar</h2>		
		<?php print $sidebar2; ?>
		</div>
	<?php endif; ?>

</div> <!-- /#column-container -->

<!--                 Page Footer                          -->
<footer class="clearfix">
<div id="footer-top" class="clearfix">
	<?php if ($page['footer_top_left']): ?>
	<section>
	<h1 class="element-invisible">Footer Top Left</h1>
	<div class="region region-footer-top-left">
	<?php print render($page['footer_top_left']); ?>
	</div>
	</section>
	<?php endif; ?>
	<?php if ($page['footer_top_right']): ?>
	<section>
	<h1 class="element-invisible">Footer Top Right</h1>
	<div class="region region-footer-top-right">
	<?php print render($page['footer_top_right']); ?>
	</div>
	</section>
	<?php endif; ?>
</div>  <!--  /#footer-top -->
<div id="footer-bottom" class="clearfix">
	<?php if ($page['footer_bottom_left']): ?>
	<section>
	<h1 class="element-invisible">Footer Bottom Left</h1>
	<div class="region region-footer-bottom-left">
	<?php print render($page['footer_bottom_left']); ?>
	</div>
	</section>
	<?php endif; ?>	
	<?php if ($page['footer_bottom_right']): ?>
	<section>
	<h1 class="element-invisible">Footer Bottom Right</h1>
	<div class="region region-footer-bottom-right">
	<?php print render($page['footer_bottom_right']); ?>
	</div>
	</section>
	<?php endif; ?>
</div>  <!--  /#footer-top -->
</footer>

<!--                     EOPAGE                          -->
